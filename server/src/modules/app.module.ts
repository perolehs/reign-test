import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ArticlesModule } from './articles/articles.module';

@Module({
  imports: [
    ArticlesModule,
    MongooseModule.forRoot('mongodb://mongo:27017/articles'),
    ScheduleModule.forRoot()
  ]
})
export class AppModule { }
