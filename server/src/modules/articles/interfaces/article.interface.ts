import { Date, Document } from 'mongoose';

export interface Article extends Document {
    readonly objectID: string;
    readonly title: string;
    readonly url: string;
    readonly author: string;
    readonly created_at: number;
    deleted: Boolean;
}

export interface ArticlesRaw {
    hits: ArticleRaw[];
}

export interface ArticleRaw {
    created_at: string;
    author: string;
    title: string | null;
    url: string | null;
    story_title: string | null;
    story_url: string | null;
    objectID: string;
}