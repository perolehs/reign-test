import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { Article, ArticleDocument } from './schemas/article.schema';
import { CreatArticleDto } from "./dto/create-article.dto";
import { Cron } from '@nestjs/schedule';
import { ArticlesRaw } from './interfaces/article.interface';

const HN_URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

@Injectable()
export class ArticlesService implements OnModuleInit {

    constructor(
        @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
        @InjectConnection() private connection: Connection,
        private httpService: HttpService
    ) { }

    onModuleInit(): void {
        this.taskHackerNewsFeed();
    }

    async createMany(articles: CreatArticleDto[]) {
        const session = await this.connection.startSession();
        session.startTransaction();
        try {
            for (let article of articles) {
                if (await this.articleModel.findOne({ objectID: article.objectID })) continue;
                const createdArticle = new this.articleModel(article);
                await createdArticle.save();
            }
            await session.commitTransaction();
        } catch (err) {
            console.log(err);
            await session.abortTransaction();
        } finally {
            await session.endSession();
        }
    }

    async findAll(): Promise<Article[]> {
        return await this.articleModel.find({ deleted: false }).exec();
    }

    async delete(objectID: string): Promise<boolean> {
        return await this.articleModel.updateOne({ objectID }, { deleted: true });
    } 

    @Cron('* * * *')
    async taskHackerNewsFeed(): Promise<void> {
        try {
            const axiosFeed = await this.httpService.get<ArticlesRaw>(HN_URL).toPromise();
            const articlesRaw = axiosFeed.data;
            const articles = articlesRaw.hits
                .filter(articleRaw => {
                    if (!articleRaw.title) {
                        articleRaw.title = articleRaw.story_title;
                        articleRaw.url = articleRaw.story_url;
                    }

                    return articleRaw.title && articleRaw.url;
                })

                .map<Article>(articleRaw => ({
                    objectID: articleRaw.objectID,
                    title: articleRaw.title,
                    author: articleRaw.author,
                    url: articleRaw.url,
                    created_at: new Date(articleRaw.created_at).getTime(),
                    deleted: false
                }));
            this.createMany(articles);
        } catch (error) {
            console.error(error);
        }
    }


}
