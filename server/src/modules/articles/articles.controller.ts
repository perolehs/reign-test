import { Body, Controller, Delete, Get, Param } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { Article } from './schemas/article.schema';

@Controller('articles')
export class ArticlesController {
    constructor(private readonly articlesService: ArticlesService) { }

    @Delete('/:objectID')
    async create(@Param('objectID') ObjectID: string) {
        return this.articlesService.delete(ObjectID);
    }

    @Get()
    async findAll(): Promise<Article[]> {
        return this.articlesService.findAll();
    }
}
