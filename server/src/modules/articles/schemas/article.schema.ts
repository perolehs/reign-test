import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
    @Prop({ unique: true, required: true, type: String })
    objectID: string;

    @Prop({ required: true })
    title: string;

    @Prop({ required: true })
    url: string; 

    @Prop({ required: true })
    author: string;
 
    @Prop({ required: true })
    created_at: number;

    @Prop({ default: false })
    deleted: Boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);