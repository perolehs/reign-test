export class CreatArticleDto {
    readonly objectID: string;
    readonly title: string;
    readonly author: string;
    readonly url: string;
    readonly created_at: number;
    deleted: Boolean;
  }