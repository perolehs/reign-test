export interface Article {
    objectID: string;
    title: string;
    url: string;
    author: string;
    created_at: number;
    deleted: Boolean;
}