import { Pipe, PipeTransform } from '@angular/core';
import * as moment from "moment";

@Pipe({
  name: 'articleDate'
})
export class ArticleDatePipe implements PipeTransform {

  transform(articledate: number): string {

    const yesterday = new Date(Date.now());
    yesterday.setDate(yesterday.getDate() - 1);

    const date = new Date(articledate);

    console.log(yesterday.getDate() < date.getDate())

    if (yesterday.getDate() === date.getDate()) {
      return 'Yesterday';
    } else if (yesterday < date) {
      return moment(date).format('LT');
    } else {
      return moment(date).format('MMM D');
    }
  }

}
