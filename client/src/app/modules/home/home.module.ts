import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { ArticleDatePipe } from 'src/app/core/pipes/article-date.pipe';



@NgModule({
  declarations: [HomeComponent, ArticleDatePipe],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
