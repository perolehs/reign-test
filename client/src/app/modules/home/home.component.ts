import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/core/interfaces';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public articles: Article[];

  constructor(
    private homeService: HomeService
  ) { 
    this.articles = [];
  }

  async ngOnInit(): Promise<void> {
    this.articles.push(...(await this.homeService.getArticles()));
  }

  public async deleteArticle(objectID: string, i: number){
    if ((await this.homeService.deleteArticles(objectID))?.nModified === 1){
      this.articles[i].deleted = true;
    }
  }

  public navTo(url:string):void{
    window.open(url, '_blank');
  }

  public isYesterday(){
    
  }

}
