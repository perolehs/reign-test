import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from 'src/app/core/interfaces';
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private serverURL = environment.backendURI.base;

  constructor(private http: HttpClient) { }

  public getArticles() {
    return this.http.get<Article[]>(`${this.serverURL}/articles`).toPromise();
  }

  public deleteArticles(objectID: string) {
    return this.http.delete<{
      n: number
      nModified: number
      ok: number
    }>(`${this.serverURL}/articles/${objectID}`).toPromise();
  }

}
